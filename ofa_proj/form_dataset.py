from PIL import Image
from torchvision import transforms
from transformers import OFATokenizer, OFAModel
import torch
from datasets import Dataset
import pandas as pd
from transformers import OFATokenizer, OFAModel
ckpt_dir="/home/v-minkeyu/src/OFA_test/OFA-base"
tokenizer = OFATokenizer.from_pretrained(ckpt_dir)
mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]
resolution = 384
pic_path = "/home/v-minkeyu/src/OFA_test/nlvr/nlvr2/data/test1/"
save_path = "/home/v-minkeyu/src/OFA_test/nlvr_test"
data=pd.read_json("test_clean.json",orient="split")
data=Dataset.from_pandas(data)
patch_resize_transform = transforms.Compose([
        lambda image: image.convert("RGB"),
        transforms.Resize((resolution, resolution//2), interpolation=Image.BICUBIC),
        transforms.ToTensor(), 
        transforms.Normalize(mean=mean, std=std)
    ])

def merge_picture(pic1,pic2):
    patch_img1 = patch_resize_transform(pic1)
    patch_img2 = patch_resize_transform(pic2)
    patch_img=torch.cat((patch_img1,patch_img2),dim=-1)
    return patch_img.unsqueeze(0)

def insert_image(data,tokenizer,pic_path):
    split_id = data["identifier"].split("-")
    name = "-".join(split_id[:3])
    pic1=Image.open(pic_path+name+"-img0.png")
    pic2=Image.open(pic_path+name+"-img1.png")
    tokens=tokenizer(data['sentence']).input_ids
    picture=merge_picture(pic1,pic2)
    data["picture"]=picture
    data["tokens"]=tokens
    return data

data=data.map(lambda d: insert_image(d,tokenizer=tokenizer,pic_path=pic_path))
data.save_to_disk(save_path)